package module

import javax.inject.Named

import com.google.inject.{AbstractModule, Provides}

/**
  * Created by tomasz on 10.05.17.
  */
class SelectorsModule extends AbstractModule {

  @Provides()
  @Named("contact-form-selectors")
  def provideSelectorsForContactFormModule(): Map[String, String] = {
    Map(
      "email-input" -> "#email-input",
      "message-input" -> "#message-input",
      "send-message-button" -> "#send-message-button",
      "show-sent-messages-link" -> "html/body/a[1]",
      "return-to-the-homepage" -> "html/body/a[2]"
    )
  }

  def provideSelectorsForOrderFormModule(): Map[String, String] = {
    Map(
      "email-input" -> "#first-name-input",
      "surname-input" -> "#surname-input"
    )
  }

  override def configure(): Unit = {
    //
  }
}
