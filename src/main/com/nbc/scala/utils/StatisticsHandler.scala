package utils

/**
  * Created by tomasz on 10.05.17.
  */
class StatisticsHandler {

  def getExecutionTime(functionToBeExecuted: () => Unit): Long = {

    val startTime: Long = System.currentTimeMillis()

    functionToBeExecuted()

    System.currentTimeMillis() - startTime
  }

  def calculateMean(executionTimes: List[Long]): Long = {
    executionTimes.sum / executionTimes.size
  }
}
