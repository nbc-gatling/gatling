import io.gatling.app.Gatling
import io.gatling.core.config.GatlingPropertiesBuilder
import simulations.E2ESimulation
/**
  * Created by tomasz on 10.05.17.
  */
object SimulationsRunner {

  def main(args: Array[String]): Unit = {

    val configuration = new GatlingPropertiesBuilder()

    configuration
      .simulationClass(classOf[E2ESimulation].getName)

    Gatling.fromMap(configuration.build)

  }
}
