package simulations

import com.google.inject.Guice
import io.gatling.core.Predef.{Simulation, _}
import io.gatling.core.structure.ScenarioBuilder
import module.SelectorsModule
import pages.LandingPage
import utils.StatisticsHandler

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * Created by tomasz on 10.05.17.
  */
class E2ESimulation extends Simulation {

  private val executionTimes: ListBuffer[Long] = ListBuffer()

  private val landingPage: LandingPage = Guice
    .createInjector(new SelectorsModule())
    .getInstance(classOf[LandingPage])

  private val statisticsCollector: StatisticsHandler = Guice
    .createInjector(new SelectorsModule())
    .getInstance(classOf[StatisticsHandler])

  val scn: ScenarioBuilder = scenario("Sending new contact message")
    .exec(session => {

      print(session)

      executionTimes +=
      statisticsCollector
        .getExecutionTime(
          () => {
            landingPage
              .goToContactForm()
              .fillContactForm(session("EMAIL").toString, session("MESSAGE").toString)
              .submitContactForm()
          }
        )


      session
    })

  setUp(
    scn.inject(
     atOnceUsers(10)))
    .maxDuration(1000 seconds)

  after {
    statisticsCollector.calculateMean(executionTimes.toList)
  }
}
