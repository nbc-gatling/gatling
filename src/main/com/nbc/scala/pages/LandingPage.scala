package pages

import javax.inject.{Inject, Singleton}

import com.codeborne.selenide.Selenide
import pages.components.ContactFormComponent

/**
  * Created by tomasz on 10.05.17.
  */
@Singleton
class LandingPage @Inject() (contactFormComponent: ContactFormComponent) {

  def goToContactForm(): LandingPage = {
    Selenide.open(System.getProperty("url"))

    this
  }

  def fillContactForm(email: String, message: String): LandingPage = {

    contactFormComponent.enterEmail(email)
    contactFormComponent.enterMessage(message)

    this
  }

  def submitContactForm(): Unit = {

    contactFormComponent.submitContactForm()
  }

}
