package pages.components

import com.codeborne.selenide.Condition._
import com.codeborne.selenide.Selenide._
import com.codeborne.selenide.SelenideElement

private[pages] trait Component {
  protected def getElement(selector: String): SelenideElement = {
    if(selector.startsWith("./")) {
      return $(selector).should(be(visible), be(enabled))
    }

    $(selector).should(be(visible), be(enabled))
  }
}