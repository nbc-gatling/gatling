package pages.components

import javax.inject.{Inject, Named, Singleton}

/**
  * Created by tomasz on 10.05.17.
  */
@Singleton
class ContactFormComponent @Inject() (@Named("contact-form-selectors") selectors: Map[String, String]) extends Component {

  def enterEmail(email: String): Unit = {
    getElement(selectors("email-input")).`val`(email)
  }

  def enterMessage(message: String): Unit = {
    getElement(selectors("message-input")).`val`(message)
  }

  def submitContactForm(): Unit = {
    getElement(selectors("send-message-button")).click()
  }
}
